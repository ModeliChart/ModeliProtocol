Using the [grpc](https://grpc.io/) framework.

Compile the ModeliRpc.proto file to generate the code.
The compilation is simplified by the generate_* scripts.

Compilation requires the plugins grcp_csharp_plugin.exe / grcp_cpp_plugin.exe which are delivered with your version of the grpc framework.
The C# library and generation tool can be obtained via nuget (Grpc, Grpc.Tools & Google.Protobuf).


The easiest way to get obtain the C++ library for visual studio is using the vcpkg tool from github (use ".\vcpkg install grpc:x64-windows").
This might be outdated and switching to conan https://github.com/inexorgame/conan-grpc might be more appropiate.