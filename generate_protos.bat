md CSharp
md Cpp
protoc.exe --csharp_out CSharp  ModeliRpc.proto --grpc_out CSharp --plugin=protoc-gen-grpc=grpc_csharp_plugin.exe
protoc.exe --cpp_out Cpp  ModeliRpc.proto --grpc_out Cpp --plugin=protoc-gen-grpc=grpc_cpp_plugin.exe
pause